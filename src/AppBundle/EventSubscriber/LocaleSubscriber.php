<?php

namespace AppBundle\EventSubscriber;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class LocaleSubscriber implements EventSubscriberInterface
{
    private $defaultLocale;

    public function __construct($defaultLocale = 'en')
    {
        $this->defaultLocale = $defaultLocale;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();

        // Проверяем на наличие параметра маршрута '_locale' в пути
        if ($locale = $request->attributes->get('_locale')) {
            //Если параметр обнаружен, то устанавливаем его в сессию
            $request->getSession()->set('_locale', $locale);
        } else {
            // Если не обнаружили параметр '_locale', то локаль сохраненную в сессии
            $request->setLocale($request->getSession()->get('_locale', $this->defaultLocale));
        }
    }

    public static function getSubscribedEvents()
    {
        return array(
            // Уведомляем Symfony о том, что данный сервис должен быть зарегистрирован после сервиса, обрабатывающего локаль по умолчанию
            KernelEvents::REQUEST => array(array('onKernelRequest', 15)),
        );
    }
}
