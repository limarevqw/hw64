<?php

namespace AppBundle\Controller;
use AppBundle\Entity\Word;
use AppBundle\Form\WordFormType;
use Knp\DoctrineBehaviors\Model\Translatable\Translation;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class MainController
 * @package AppBundle\Controller
 */
class MainController extends Controller
{
    /**
     * @Route("/")
     * @Method({"GET", "HEAD", "POST"})
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $locale = $request->getLocale();

        $words = $this->getDoctrine()
            ->getRepository(Word::class)
            ->findAll();

        $formWord = $this->createForm(WordFormType::class);

        $word = new Word();

        $formWord->handleRequest($request);

        if ($formWord->isSubmitted() && $formWord->isValid()){
            $data = $formWord->getData();

            $word->translate($locale)->setText($data['text']);
            $em = $this->getDoctrine()->getManager();
            $em->persist($word);

            $word->mergeNewTranslations();

            $em->flush();

            return $this->redirectToRoute('app_main_index');
        }

        return $this->render("@App/main/index.html.twig", [
            'words' => $words,
            'locale' => $locale,
            'formWord' => $formWord->createView()
        ]);
    }

    /**
     * @Method({"GET", "POST"})
     * @Route("/show/{word_id}")
     * @param Request $request
     * @param $word_id
     * @return Response
     */
    public function showAction(Request $request, $word_id)
    {

        $locales = [
            'fr' =>  'Французский',
            'de' =>  'Немецкий',
            'kg' =>  'Кыргызский',
            'en' =>  'Английский',
            'kz' =>  'Казахский',
            'ru' =>  'Русский',
        ];

        $locale = $request->getLocale();

        $word = $this->getDoctrine()
            ->getRepository(Word::class)
            ->find($word_id);

        /** @var Translation[] $translations */
        $translations = $word->getTranslations();
        $builder = $this->createFormBuilder();

        foreach ($translations as $translation){
            unset($locales[$translation->getLocale()]);
        }

        foreach($locales as $key => $language) {
            $builder->add($key, TextType::class, [
                'required' => false,
                'label' => $language
            ]);
        }

        $builder->add('save', SubmitType::class, ['label' => 'Сохранить']);
        $formWords = $builder->getForm();
        $formWords->handleRequest($request);

        if($formWords->isSubmitted() && $formWords->isValid()) {
            $data = $formWords->getData();
            $em = $this->getDoctrine()->getManager();

            foreach ($data as $key => $value){
                if ($value != null){
                    $word->translate($key)->setText($value);
                }
            }

            $word->mergeNewTranslations();

            $em->flush();
            return $this->redirectToRoute('app_main_show', [
                'word_id' => $word_id
            ]);
        }

        return $this->render('@App/main/show.html.twig', [
            'formWords' => $formWords->createView(),
            'word' => $word,
            'CurentLocale' => $locale
        ]);
    }

    /**
     * @Route("/{_locale}", requirements = {"_locale" : "en|ru"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function changeLocal(Request $request)
    {
        return $this->redirect($request->headers->get('referer'));
    }

}
